﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Newton
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INewton" in both code and config file together.
    [ServiceContract]
    public interface INewton
    {
        [OperationContract]
        List<spListAllPagesResult> ListAllPages(int PortalId, string KeyPar1, string KeyPar2, string Culture, bool ShowUnpublisheds);
        [OperationContract]
        List<spListCategoriesResult> ListCategories(int PortalId, string KeyPar1, string KeyPar2, string Culture, int ParentId, bool ShowInactives);
        [OperationContract]
        int GetPageId(int PortalId, string KeyPar1, string KeyPar2, string Tag);
        [OperationContract]
        List<spGetPageResult> GetPage(int PortalId, string KeyPar1, string KeyPar2, int PageId);
        [OperationContract]
        string GetParameter(int PortalId, string KeyPar1, string KeyPar2, string ParameterId);
        [OperationContract]
        List<spListAllParametersResult> ListAllParameters(int PortalId, string KeyPar1, string KeyPar2);
        [OperationContract]
        List<spListPagesOnCategoryResult> ListPagesOnCategory(int PortalId, string KeyPar1, string KeyPar2, string Culture, int CategoryId, bool ShowUnpublisheds);
        [OperationContract]
        int GetCategoryId(int PortalId, string KeyPar1, string KeyPar2, string Tag);
        [OperationContract]
        List<spGetCategoryResult> GetCategory(int PortalId, string KeyPar1, string KeyPar2, int CategoryId);
        [OperationContract]
        List<spListRelatedPagesResult> ListRelatedPages(int PortalId, string KeyPar1, string KeyPar2, int PageId);
    }
}
