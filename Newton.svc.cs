﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Newton
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Newton" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Newton.svc or Newton.svc.cs at the Solution Explorer and start debugging.
    public class Newton : INewton
    {
        public List<spListAllPagesResult> ListAllPages(int PortalId, string KeyPar1, string KeyPar2, string Culture, bool ShowUnpublisheds)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            return SqlServer.spListAllPages(PortalId, Culture, ShowUnpublisheds).ToList();
        }
        public List<spListCategoriesResult> ListCategories(int PortalId, string KeyPar1, string KeyPar2, string Culture, int ParentId, bool ShowInactives)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            return SqlServer.spListCategories(PortalId, Culture, ParentId, ShowInactives).ToList();
        }
        public int GetPageId(int PortalId, string KeyPar1, string KeyPar2, string Tag)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            int r = (from c in SqlServer.PAGEs
                     where c.PortalId.Equals(PortalId)
                     where c.Tag.Equals(Tag)
                     select c.PageId).Single();
            int ret = 0;
            if (r != null)
            {
                ret = r;
            }
            return r;
        }
        public List<spGetPageResult> GetPage(int PortalId, string KeyPar1, string KeyPar2, int PageId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            return SqlServer.spGetPage(PortalId, PageId).ToList();
        }
        public List<spListAllParametersResult> ListAllParameters(int PortalId, string KeyPar1, string KeyPar2)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            return SqlServer.spListAllParameters(PortalId).ToList();
        }
        public string GetParameter(int PortalId, string KeyPar1, string KeyPar2, string ParameterId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            var r = (from c in SqlServer.PORTAL_PARAMETERs
                     where c.PortalId.Equals(PortalId)
                     where c.ParameterId.Equals(ParameterId)
                     select c.Value).Single();
            string ret = "";
            if (r != null)
            {
                ret = r;
            }
            return r;
        }
        public List<spListPagesOnCategoryResult> ListPagesOnCategory(int PortalId, string KeyPar1, string KeyPar2, string Culture, int CategoryId, bool ShowUnpublisheds)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            return SqlServer.spListPagesOnCategory(PortalId, Culture, CategoryId, ShowUnpublisheds).ToList();
        }
        public int GetCategoryId(int PortalId, string KeyPar1, string KeyPar2, string Tag)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            int ret;
            try
            {
                ret = (from c in SqlServer.CATEGORies
                       where c.PortalId.Equals(PortalId)
                       where c.Tag.Equals(Tag)
                       select c.CategoryId).Single();
            }
            catch (Exception ex) {
                ret = 0;
            }
            return ret;
        }
        public List<spGetCategoryResult> GetCategory(int PortalId, string KeyPar1, string KeyPar2, int CategoryId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            return SqlServer.spGetCategory(PortalId, CategoryId).ToList();
        }
        public List<spListRelatedPagesResult> ListRelatedPages(int PortalId, string KeyPar1, string KeyPar2, int PageId)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            return SqlServer.spListRelatedPages(PortalId, PageId).ToList();
        }
    }
}
