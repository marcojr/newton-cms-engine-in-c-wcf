﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/MasterPage.Master" AutoEventWireup="true" CodeBehind="AssignRelatedPage.aspx.cs" Inherits="Newton.backoffice.AssignRelatedPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="ContentPlaceHolder1">
    <strong><span style="font-size: large">
    <br />
    Page Assignments:<br />
    <br />
    </span>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
    <table style="width: 100%">
        <tr>
            <td style="width: 63px">Culture:</td>
            <td>
                <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True">
                    <asp:ListItem>en-US</asp:ListItem>
                    <asp:ListItem>pt-BR</asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
    </table>
    </strong>
            <br />
    <table width="100%">
        <tr>
            <td width="50%" valign="top">Select Page:<br />
                <br />
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True">
                </asp:RadioButtonList>
            </td>
            <td valign="top">Select Related Pages:<br />
                <br />
                <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                </asp:CheckBoxList>
              
            </td>
        </tr>
    </table><br />
            <Center>
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Assign" />
            </Center><br /><br />
            <br />
    </ContentTemplate></asp:UpdatePanel>
</asp:Content>
