﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Newton.backoffice
{
    public partial class AssignRelatedPage : System.Web.UI.Page
    {
        public string assignments;
        protected void Page_Load(object sender, EventArgs e)
        {
            string cult;
            if (!IsPostBack)
            {
                cult = "pt-BR";
                DropDownList1.Items.FindByValue(cult).Selected = true;
            }
            else
            {
                cult = DropDownList1.SelectedItem.Value;
            }
            int Pid = 1;
            LoadPages1(Pid, cult);
            LoadPages2(Pid, cult);
        }
        protected void LoadPages1(int Pid, string cult)
        {
            assignments = "";
            for (int i = 0; i < CheckBoxList1.Items.Count; i++)
            {
                if (CheckBoxList1.Items[i].Selected)
                {
                    assignments = assignments + "," + CheckBoxList1.Items[i].Value;
                }
            }
            if (assignments.Length > 0)
            {
                assignments = assignments.Substring(1, assignments.Length - 1);
            }
            CheckBoxList1.Items.Clear();
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            var pages = SqlServer.spListAllPages(Pid, cult, true).ToArray();
            for (int i = 0; i < pages.Length; i++)
            {
                CheckBoxList1.Items.Add(new ListItem(pages[i].Title, pages[i].PageId.ToString()));
            }
            if (RadioButtonList1.SelectedItem != null)
            {
                CheckBoxList1.Items.FindByValue(RadioButtonList1.SelectedItem.Value).Enabled = false;
                var sels = SqlServer.spListRelatedPages(Pid, Convert.ToInt16(RadioButtonList1.SelectedItem.Value)).ToArray();
                for (int i = 0; i < sels.Length; i++)
                {
                    CheckBoxList1.Items.FindByValue(sels[i].PageId.ToString()).Selected = true;
                }
            }
        }
        protected void LoadPages2(int Pid, string cult)
        {
            string pointer = "";
            if (RadioButtonList1.SelectedItem != null)
            {
                pointer = RadioButtonList1.SelectedItem.Value;
            }
            RadioButtonList1.Items.Clear();
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            var pages = SqlServer.spListAllPages(Pid, cult, true).ToArray();
            for (int i = 0; i < pages.Length; i++)
            {
                RadioButtonList1.Items.Add(new ListItem(pages[i].Title, pages[i].PageId.ToString()));
            }
            if (pointer != "")
            {
                RadioButtonList1.Items.FindByValue(pointer).Selected = true;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            string[] pages = assignments.Split(',');
            int Pid = 1;
            for (int i = 0; i < CheckBoxList1.Items.Count; i++)
            {
                SqlServer.spUnassignRelatedPage(Pid, Convert.ToInt16(CheckBoxList1.Items[i].Value), Convert.ToInt16(RadioButtonList1.SelectedItem.Value));
            }
            for (int i = 0; i < pages.Length; i++)
            {
                SqlServer.spAssignRelatedPage(Pid, Convert.ToInt16(pages[i]), Convert.ToInt16(RadioButtonList1.SelectedItem.Value));
            }
            Response.Redirect("default.aspx");
        }
    }
}