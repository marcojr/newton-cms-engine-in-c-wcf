﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Newton.backoffice
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["bk_PortalId"] = "1";
            LoadPars();

        }
        protected void LoadPars()
        {
            SqlServerDataContext SqlServer = new SqlServerDataContext();
            int Pid = Convert.ToInt16(Session["bk_PortalId"]);
            var p = SqlServer.spListAllParameters(Pid).ToArray();
            for (int i = 0; i < p.Length; i++)
            {
                Session["PAR_" + p[i].ParameterId] = p[i].Value;
            }
            Session["Culture"] = Session["PAR_DEFAULT_CULTURE"].ToString();
        }
    }
}